(* ========================================================================= *)
(* Further tactics for HOL Light                                             *)
(*                                                                           *)
(* Copyright (c) 2014-2015 Marco Maggesi                                     *)
(* ========================================================================= *)

needs "Snippets/rules.hl";;

let CASES_TAC tm =
  let tyname = fst (dest_type (type_of tm)) in
  STRUCT_CASES_TAC (ISPEC tm (cases tyname));;

let GEN_THEN : (term -> tactic) -> tactic =
  fun tac (asl,w) ->
    try let x = fst(dest_forall w) in
        let avoids = itlist (union o thm_frees o snd) asl (frees w) in
        let x' = mk_primed_var avoids x in
        (X_GEN_TAC x' THEN tac x') (asl,w)
    with Failure _ -> failwith "GEN_THEN";;

let STRUCT_GEN_TAC th : tactic=
  GEN_THEN (fun v -> STRUCT_CASES_TAC (ISPEC v th));;

(* ------------------------------------------------------------------------- *)
(* Further tactics for structuring the proof flow.                           *)
(* ------------------------------------------------------------------------- *)

let EXFALSO_TAC : tactic =
  MATCH_MP_TAC (MESON[] `!p. F ==> p`);;

let CUT_TAC : term -> tactic =
  let th = MESON [] `(p ==> q) /\ p ==> q`
  and ptm = `p:bool` in
  fun tm -> MATCH_MP_TAC (INST [tm,ptm] th) THEN CONJ_TAC;;

let LIST_MP_TAC : thm list -> tactic = MP_TAC o CONJ_LIST;;

let GEN_EQ_TAC : tactic =
  CONV_TAC GEQ_CONV THEN CONJ_TAC;;
