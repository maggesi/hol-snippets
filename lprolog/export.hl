(* ========================================================================= *)
(* Export HOL types, terms and theorems in lambda prolog notation.           *)
(*                                                                           *)
(* Copyright (c) 2016 Marco Maggesi                                          *)
(* ========================================================================= *)

let pp_print_qstring fmt s =
  pp_print_char fmt '"'; pp_print_string fmt (String.escaped s);
  pp_print_char fmt '"';;

let pp_print_list pp fmt =
  let rec recur =
    function
      [] -> pp_print_string fmt "]"; pp_close_box fmt ()
    | x :: xs -> pp_print_string fmt ","; pp_print_space fmt ();
                 pp fmt x; recur xs in
  function
    [] -> pp_print_string fmt "[]"
  | x :: xs -> pp_open_box fmt 1; pp_print_string fmt "["; pp fmt x; recur xs;;

let rec lprolog_print_type fmt =
  function
    Tyvar s ->
      pp_open_box fmt 2; pp_print_string fmt "(tyvarv"; pp_print_space fmt ();
      pp_print_qstring fmt s; pp_print_string fmt ")"; pp_close_box fmt ()
  | Tyapp(s,tyl) ->
      pp_open_box fmt 2; pp_print_string fmt "(tyapp"; pp_print_space fmt ();
      pp_print_qstring fmt s; pp_print_space fmt ();
      pp_print_list lprolog_print_type fmt tyl;
      pp_print_string fmt ")"; pp_close_box fmt ();;

let rec lprolog_print_term fmt =
  function
    Var(s,ty) ->
      pp_open_box fmt 2; pp_print_string fmt "(var"; pp_print_space fmt ();
      pp_print_qstring fmt s; pp_print_space fmt (); lprolog_print_type fmt ty;
      pp_print_string fmt ")"; pp_close_box fmt ()
  | Const(s,ty) ->
      pp_open_box fmt 2; pp_print_string fmt "(const"; pp_print_space fmt ();
      pp_print_qstring fmt s; pp_print_space fmt (); lprolog_print_type fmt ty;
      pp_print_string fmt ")"; pp_close_box fmt ()
  | Comb(f,x) ->
      pp_open_box fmt 2; pp_print_string fmt "(comb"; pp_print_space fmt ();
      lprolog_print_term fmt f; pp_print_space fmt (); lprolog_print_term fmt x;
      pp_print_string fmt ")"; pp_close_box fmt ()
  | Abs(v,b) ->
      pp_open_box fmt 2; pp_print_string fmt "(abs"; pp_print_space fmt ();
      lprolog_print_term fmt v; pp_print_space fmt (); lprolog_print_term fmt b;
      pp_print_string fmt ")"; pp_close_box fmt ();;

let lprolog_print_thm fmt thm =
  pp_open_box fmt 2; pp_print_string fmt "(sequent"; pp_print_space fmt ();
  pp_print_list lprolog_print_term fmt (hyp thm); pp_print_space fmt ();
  lprolog_print_term fmt (concl thm); pp_print_string fmt ")";
  pp_close_box fmt ();;

let lprolog_print_named_thm fmt (s,th) =
  pp_open_box fmt 2; pp_print_string fmt "(thm"; pp_print_space fmt ();
  pp_print_qstring fmt s; pp_print_space fmt (); lprolog_print_thm fmt th;
  pp_close_box fmt (); pp_print_string fmt ")";;

let elpi_print_type = lprolog_print_type std_formatter
and elpi_print_term = lprolog_print_term std_formatter
and elpi_print_thm = lprolog_print_thm std_formatter;;

(* ------------------------------------------------------------------------- *)
(* Dump the database.                                                        *)
(* ------------------------------------------------------------------------- *)

let lprolog_print_theorems fmt thl =
  let f (s,v) =
    lprolog_print_named_thm fmt (s,v);
    pp_print_string fmt "."; pp_print_newline fmt (); in
  do_list f thl;;

let lprolog_dump fname =
  let oc = open_out fname in
  let fmt = formatter_of_out_channel oc in
  pp_set_margin fmt 150;
  lprolog_print_theorems fmt (search[]);
  close_out oc;;

(*;;
lprolog_dump "theorems_hypercomplex.mod";;
*)
