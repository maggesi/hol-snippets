(* ========================================================================= *)
(* Experiments in plane geometry.                                            *)
(* ========================================================================= *)

(* ------------------------------------------------------------------------- *)
(* A few easy results missing(?) from the standard library.                  *)
(* ------------------------------------------------------------------------- *)

let MIDPOINT_COMPONENT = prove
 (`!x y z:real^N i. midpoint (x,y)$i = inv(&2) * (x$i + y$i)`,
  REWRITE_TAC[midpoint; VECTOR_MUL_COMPONENT; VECTOR_ADD_COMPONENT]);;

let DIST_POW_2 = prove
 (`!x y:real^N.
     dist (x,y) pow 2 = sum (1..dimindex(:N)) (\i. (x$i - y$i) pow 2)`,
  REWRITE_TAC[dist; NORM_POW_2; dot; GSYM REAL_POW_2; VECTOR_SUB_COMPONENT]);;

let DIST_EQ_SQUARE = prove
 (`!x y:real^N e. dist(x,y) = e <=> &0 <= e /\ dist(x,y) pow 2 = e pow 2`,
  REWRITE_TAC[dist; NORM_EQ_SQUARE; NORM_POW_2]);;

let IN_SPHERE = prove
 (`!x y e. y IN sphere(x,e) <=> dist(x,y) = e`,
  REWRITE_TAC[sphere; IN_ELIM_THM]);;

let IN_SPHERE_SQUARE = prove
 (`!x y e. y IN sphere(x,e) <=> &0 <= e /\ dist(x,y) pow 2 = e pow 2`,
  REWRITE_TAC[IN_SPHERE; DIST_EQ_SQUARE]);;

(* ------------------------------------------------------------------------- *)
(* Instantiation of general theorems to the 2D case.                         *)
(* ------------------------------------------------------------------------- *)

let CART_EQ_2D = prove
 (`!x y:A^2. x = y <=> x$1 = y$1 /\ x$2 = y$2`,
  REWRITE_TAC[CART_EQ; DIMINDEX_2; FORALL_2]);;

let NORM_POW_2_2D = prove
 (`!x:real^2. norm x pow 2 = x$1 pow 2 + x$2 pow 2`,
  REWRITE_TAC[NORM_POW_2; DOT_2; DIMINDEX_2; SUM_2; REAL_POW_2]);;

let DIST_POW_2_2D = prove
 (`!x y:real^2. dist(x,y) pow 2 = (x$1 - y$1) pow 2 + (x$2 - y$2) pow 2`,
  REWRITE_TAC[DIST_POW_2; DIMINDEX_2; SUM_2]);;

let DIST_EQ_2D = prove
 (`!x y:real^2 z w:real^2.
     dist(x,y) = dist(z,w) <=>
     (x$1 - y$1) pow 2 + (x$2 - y$2) pow 2 =
     (z$1 - w$1) pow 2 + (z$2 - w$2) pow 2`,
  REWRITE_TAC[DIST_EQ; DIST_POW_2_2D]);;

let IN_SPHERE_2D = prove
 (`!x y:real^2 e.
      y IN sphere(x,e) <=>
      &0 <= e /\ (x$1 - y$1) pow 2 + (x$2 - y$2) pow 2 = e pow 2`,
  REWRITE_TAC[IN_SPHERE_SQUARE; DIST_POW_2_2D]);;

let ORTHOGONAL_2D = prove
 (`!x y:real^2. orthogonal x y <=> x$1 * y$1 + x$2 * y$2 = &0`,
  REWRITE_TAC[orthogonal; DOT_2]);;

let BASIS_COMPONENT_2D = prove
 (`basis 1:real^2$1 = &1 /\ basis 1:real^2$2 = &0 /\
   basis 2:real^2$1 = &0 /\ basis 2:real^2$2 = &1`,
  SIMP_TAC[DIMINDEX_2; ARITH_RULE `1 <= 2`; LE_REFL; BASIS_COMPONENT] THEN
  REWRITE_TAC[ARITH_RULE `~(2 = 1)`]);;

(* ------------------------------------------------------------------------- *)
(* A conversion (and its associated tactic) to reduce a coordinate-free,     *)
(* synthetic sentence of plane geometry to its equivalent analytic form.     *)
(* ------------------------------------------------------------------------- *)

let EXPAND_2D_CONV =
  PURE_ONCE_REWRITE_CONV
    [CART_EQ_2D; COLLINEAR_3_2D; DIST_EQ_2D; IN_SPHERE_2D; ORTHOGONAL_2D] THENC
  PURE_REWRITE_CONV
    [VECTOR_ADD_COMPONENT; VECTOR_SUB_COMPONENT; VECTOR_MUL_COMPONENT;
     MIDPOINT_COMPONENT; VEC_COMPONENT; VECTOR_2; BASIS_COMPONENT_2D;
     DIST_POW_2_2D; NORM_POW_2_2D];;

let EXPAND_2D_TAC = CONV_TAC EXPAND_2D_CONV;;

(* ------------------------------------------------------------------------- *)
(* Example: The diagonals of a parallelogram bisect each other.              *)
(* ------------------------------------------------------------------------- *)

g `!a b c d x:real^2.
     b - a = c - d /\ ~collinear{a,b,c} /\
     collinear {a,x,c} /\ collinear {b,x,d}
     ==> dist(x,a) = dist(x,c) /\ dist(x,b) = dist(x,d)`;;
e EXPAND_2D_TAC;;
time e (CONV_TAC REAL_RING);;
top_thm();;

(* ------------------------------------------------------------------------- *)
(* Alternative proof using WLOG to reduce the number of parameters.          *)
(* ------------------------------------------------------------------------- *)

g `!a b c d x:real^2.
     b - a = c - d /\ ~collinear{a,b,c} /\
     collinear {a,x,c} /\ collinear {b,x,d}
     ==> dist(x,a) = dist(x,c) /\ dist(x,b) = dist(x,d)`;;
e (GEOM_ORIGIN_TAC `a:real^2`);;
e (GEOM_BASIS_MULTIPLE_TAC 1 `b:real^2`);;
e EXPAND_2D_TAC;;
time e (CONV_TAC REAL_RING);;
top_thm();;

(* ------------------------------------------------------------------------- *)
(* Alternative version: uses `midpoint` instead of distance.                 *)
(* The resultin problem is linear: can be solved with VECTOR_ARITH.          *)
(* It works in arbitrary dimension.                                          *)
(* ------------------------------------------------------------------------- *)

g `!a b c d:real^N. b - a = c - d ==> midpoint (a,c) = midpoint (b,d)`;;
e (REWRITE_TAC[midpoint]);;
time e VECTOR_ARITH_TAC;;
top_thm();;

(* ------------------------------------------------------------------------- *)
(* The altitude to the base of an isosceles triangle bisects the base.       *)
(* ------------------------------------------------------------------------- *)

g `!a b c h:real^2.
     dist(a,b) = dist (a,c) /\
     collinear{b,h,c} /\ orthogonal (a - h) (c - b)
     ==> dist(b,h) = dist(h,c)`;;
e (GEOM_ORIGIN_TAC `a:real^2`);;
e (GEOM_BASIS_MULTIPLE_TAC 1 `h:real^2`);;
e EXPAND_2D_TAC;;
time e (CONV_TAC REAL_RING);;
top_thm();;

(* ------------------------------------------------------------------------- *)
(* Alternative version: uses `midpoint` instead of distance.                 *)
(* ------------------------------------------------------------------------- *)

g `!a b c h:real^2.
     dist(a,b) = dist (a,c) /\ ~(b = c) /\
     collinear{b,h,c} /\ orthogonal (a - h) (c - b)
     ==> h = midpoint (b,c)`;;
e (REPEAT STRIP_TAC);;
e (CUT_TAC `dist(b,h:real^2) = dist(h,c)`);;
e (ASM_MESON_TAC[MIDPOINT_COLLINEAR]);;
e (POP_ASSUM_LIST (MP_TAC o CONJ_LIST));;
e (GEOM_ORIGIN_TAC `a:real^2`);;
e (GEOM_BASIS_MULTIPLE_TAC 1 `h:real^2`);;
e EXPAND_2D_TAC;;
time e (CONV_TAC REAL_RING);;
top_thm();;

(* ------------------------------------------------------------------------- *)
(* Alternative proof using REAL_SOS.                                         *)
(* ------------------------------------------------------------------------- *)

needs "Examples/sos.ml";;

g `!a b c h:real^2.
     dist(a,b) = dist (a,c) /\ ~(b = c) /\
     collinear{b,h,c} /\ orthogonal (a - h) (c - b)
     ==> h = midpoint (b,c)`;;
e (GEOM_ORIGIN_TAC `a:real^2`);;
e (GEOM_BASIS_MULTIPLE_TAC 1 `h:real^2`);;
e EXPAND_2D_TAC;;
time e (CONV_TAC REAL_SOS);;
top_thm();;

(* ------------------------------------------------------------------------- *)
(* Three dinstinct points on a circle are non-collinear.                     *)
(* ------------------------------------------------------------------------- *)

g `!a b c:real^2.
     ~(a = b) /\ ~(a = c) /\ ~(b = c) /\
     (?x r. a IN sphere (x,r) /\ b IN sphere (x,r) /\ c IN sphere (x,r))
     ==> ~collinear{a,b,c}`;;
e (GEOM_ORIGIN_TAC `a:real^2`);;
e (GEOM_BASIS_MULTIPLE_TAC 1 `b:real^2`);;
e EXPAND_2D_TAC;;
time e (CONV_TAC REAL_RING);;
let ON_CIRCLE_IMP_NOT_COLLINEAR_ = top_thm();;

(* ------------------------------------------------------------------------- *)
(* Existence of the circumscribed circle.                                    *) 
(* ------------------------------------------------------------------------- *)

g `!a b c:real^2.
     ~collinear{a,b,c}
     ==>
     (?x r. a IN sphere (x,r) /\ b IN sphere (x,r) /\ c IN sphere (x,r))`;;
e (GEOM_ORIGIN_TAC `a:real^2`);;
e (GEOM_BASIS_MULTIPLE_TAC 1 `b:real^2`);;
e (INTRO_TAC "!b; bpos; !c; ndeg");;
e (ABBREV_TAC `p = b / &2`);;
e (ABBREV_TAC `q = (c:real^2$1 pow 2 + c$2 pow 2 - b * c$1) / (&2 * c$2)`);;
e (ABBREV_TAC `cc = vector[p;q]:real^2`);;
e (EXISTS_TAC `cc:real^2`);;
e (EXISTS_TAC `dist (cc:real^2,vec 0)`);;
e (REPEAT (FIRST_X_ASSUM SUBST_VAR_TAC));;
e (POP_ASSUM MP_TAC);;
e EXPAND_2D_TAC;;
e (REWRITE_TAC[DIST_POS_LE]);;
e (CONV_TAC REAL_FIELD);;
let NOT_COLLINEAR_IMP_ON_CIRCLE = top_thm();;

(* ------------------------------------------------------------------------- *)
(* Feuerbach's theorem on the existence of the nine point circle.            *)
(* First we make a test with a proof of a much weak version on the           *)
(* "admissibility" of the nine point circle for a right triangle.            *)
(* Legenda:                                                                  *)
(*  - ABC (non-degenerate) right triangle;                                   *)
(*  - A right angle;                                                         *)
(*  - h foot of the altitude.                                                *)
(* ------------------------------------------------------------------------- *)

g `!a b c h x:real^2 r.
     ~collinear{a,b,c} /\
     orthogonal (b - a) (c - a) /\
     collinear{b,c,h} /\
     orthogonal (a - h) (b - c) /\
     midpoint (a,b) IN sphere (x,r) /\
     midpoint (a,c) IN sphere (x,r) /\
     midpoint (b,c) IN sphere (x,r)
     ==> a IN sphere (x,r) /\ h IN sphere (x,r)`;;
e (GEOM_ORIGIN_TAC `a:real^2`);;
e (GEOM_BASIS_MULTIPLE_TAC 1 `b:real^2`);;
e EXPAND_2D_TAC;;
e (SIMP_TAC[]);;
time e (CONV_TAC REAL_RING);;
top_thm();;

(* ------------------------------------------------------------------------- *)
(* Feuerbach's theorem on the existence of the nine-point circle.            *)
(* The full version.                                                         *)
(* ------------------------------------------------------------------------- *)

g `!a b c h k l p:real^2.
     ~collinear{a,b,c} /\
     collinear{b,c,h} /\ orthogonal (a - h) (b - c) /\
     collinear{c,a,k} /\ orthogonal (b - k) (c - a) /\
     collinear{a,b,l} /\ orthogonal (c - l) (a - b) /\
     collinear{a,p,h} /\ collinear{b,p,k} /\ collinear{c,p,l}
     ==> ?x r.
           midpoint (a,b) IN sphere (x,r) /\
           midpoint (c,a) IN sphere (x,r) /\
           midpoint (b,c) IN sphere (x,r) /\
           h IN sphere (x,r) /\ k IN sphere (x,r) /\ l IN sphere (x,r) /\
           midpoint (a,p) IN sphere (x,r) /\
           midpoint (b,p) IN sphere (x,r) /\
           midpoint (c,p) IN sphere (x,r)`;;
e (INTRO_TAC "!a b c h k l p; ndeg h1 h2 k1 k2 l1 l2 p1 p2 p3");;
e (CLAIM_TAC "@x r. circ" `?x:real^2 r. midpoint (a,b) IN sphere (x,r) /\
                                        midpoint (c,a) IN sphere (x,r) /\
                                        midpoint (b,c) IN sphere (x,r)`);;
e (MATCH_MP_TAC NOT_COLLINEAR_IMP_ON_CIRCLE);;
e (USE_THEN "ndeg" MP_TAC);;
e EXPAND_2D_TAC;;
e (CONV_TAC REAL_RING);;
e (EXISTS_TAC `x:real^2`);;
e (EXISTS_TAC `r:real`);;
e (ASM_REWRITE_TAC[]);;
e (POP_ASSUM_LIST (MP_TAC o CONJ_LIST));;
e (GEOM_ORIGIN_TAC `a:real^2`);;
e (GEOM_BASIS_MULTIPLE_TAC 1 `b:real^2`);;
e EXPAND_2D_TAC;;
e (SIMP_TAC[]);;
time e (CONV_TAC REAL_RING);;
let FEUERBACH_CIRCLE = top_thm();;
