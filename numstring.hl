(* ========================================================================= *)
(* Bijection between strings and natural numbers.                            *)
(*                                                                           *)
(* Copyright (c) 2015 Marco Maggesi                                          *)
(* ========================================================================= *)

needs "Library/card.ml";;
needs "Snippets/tactics.hl";;

(* ------------------------------------------------------------------------- *)
(* Cardinality of chars and strings.                                         *)
(* ------------------------------------------------------------------------- *)

let CHAR_ENUM =
  let th1 = prove
   (`(:char) =
     {c | ?b0 b1 b2 b3 b4 b5 b6 b7. c = ASCII b0 b1 b2 b3 b4 b5 b6 b7}`,
    REWRITE_TAC[EXTENSION; IN_UNIV] THEN STRUCT_GEN_TAC (cases "char") THEN
    REWRITE_TAC[IN_ELIM_THM] THEN
    (MAP_EVERY EXISTS_TAC o striplist dest_conj)
      `a0 /\ a1 /\ a2 /\ a3 /\ a4 /\ a5 /\ a6 /\ a7` THEN
    REWRITE_TAC[])
  and th2 =
    let th = SET_RULE
      `{c:char | c = a} = {a} /\
       !P. {x:char | ?b. P x b} = {x | P x F} UNION {x | P x T}` in
    REWRITE_CONV[th; INSERT_UNION_EQ; UNION_EMPTY]
      `{c | ?b0 b1 b2 b3 b4 b5 b6 b7. c = ASCII b0 b1 b2 b3 b4 b5 b6 b7}` in
  TRANS th1 th2;;

let HAS_SIZE_CHAR = prove
 (`(:char) HAS_SIZE 256`,
  MATCH_MP_TAC
   (ISPEC `(:bool#bool#bool#bool#bool#bool#bool#bool)` CARD_HAS_SIZE_CONG) THEN
  CONJ_TAC THENL
  [REWRITE_TAC[GSYM CROSS_UNIV; ARITH_RULE `256 = 2*2*2*2*2*2*2*2`] THEN
   REPEAT (MATCH_MP_TAC HAS_SIZE_CROSS THEN REWRITE_TAC[HAS_SIZE_BOOL]);
   REWRITE_TAC[EQ_C_BIJECTIONS; IN_UNIV] THEN
   EXISTS_TAC `\ (b0,b1,b2,b3,b4,b5,b6,b7). ASCII b0 b1 b2 b3 b4 b5 b6 b7` THEN
   EXISTS_TAC `\ (ASCII b0 b1 b2 b3 b4 b5 b6 b7). b0,b1,b2,b3,b4,b5,b6,b7` THEN
   REWRITE_TAC[FUN_EQ_THM; FORALL_PAIR_THM] THEN
   MATCH_MP_TAC char_INDUCT THEN REWRITE_TAC[]]);;

let CARD_CHAR = prove
 (`CARD (:char) = 256`,
  MATCH_MP_TAC HAS_SIZE_CARD THEN ACCEPT_TAC HAS_SIZE_CHAR);;

let FINITE_CHAR = prove
 (`FINITE (:char)`,
  REWRITE_TAC[FINITE_HAS_SIZE; CARD_CHAR; HAS_SIZE_CHAR]);;

let CARD_EQ_STRING = prove
 (`(:string) =_c (:num)`,
  GEN_REWRITE_TAC I [GSYM CARD_LE_ANTISYM] THEN CONJ_TAC THENL
  [TRANS_TAC CARD_LE_TRANS `(:num list)` THEN CONJ_TAC THENL
   [(MATCH_MP_TAC o  REWRITE_RULE[IN_UNIV; UNIV_GSPEC])
      (ISPECL [`(:char)`;`(:num)`] CARD_LE_LIST) THEN
    MATCH_MP_TAC CARD_LE_FINITE_INFINITE THEN
    REWRITE_TAC[num_INFINITE; FINITE_CHAR];
    MATCH_MP_TAC CARD_EQ_IMP_LE THEN
    MATCH_MP_TAC CARD_EQ_LIST THEN
    REWRITE_TAC[num_INFINITE]];
    REWRITE_TAC[LE_C; IN_UNIV] THEN EXISTS_TAC `LENGTH:string->num` THEN
    INDUCT_TAC THENL
    [EXISTS_TAC `"":string` THEN REWRITE_TAC[LENGTH];
     POP_ASSUM STRIP_ASSUME_TAC THEN
     EXISTS_TAC `CONS (ASCII F F F F F F F F) y` THEN
     ASM_REWRITE_TAC[LENGTH]]]);;

(* ------------------------------------------------------------------------- *)
(* CHAR_OF_NUM                                                               *)
(* ------------------------------------------------------------------------- *)

let CHAR_OF_NUM = new_definition
  `CHAR_OF_NUM n =
     ASCII (ODD (n DIV 128))
	   (ODD (n DIV 64))
	   (ODD (n DIV 32))
	   (ODD (n DIV 16))
	   (ODD (n DIV 8))
	   (ODD (n DIV 4))
           (ODD (n DIV 2))
           (ODD n)`;;

let CHAR_OF_NUM_CLAUSES =
  let char_of_num_conv = REWR_CONV CHAR_OF_NUM THENC NUM_REDUCE_CONV in
  let mk_char_of_num_tm n = mk_comb(`CHAR_OF_NUM`, mk_small_numeral n) in
  let mk_char_of_num_th n = char_of_num_conv (mk_char_of_num_tm n) in
  end_itlist CONJ (map mk_char_of_num_th (0--255));;

let CHAR_OF_NUM_CONV = GEN_REWRITE_CONV I [CHAR_OF_NUM_CLAUSES];;

(* ------------------------------------------------------------------------- *)
(* NUM_OF_CHAR                                                               *)
(* ------------------------------------------------------------------------- *)

let NUM_OF_CHAR = new_recursive_definition char_RECURSION
  `NUM_OF_CHAR (ASCII b7 b6 b5 b4 b3 b2 b1 b0) =
   NUMERAL
     ((if b0 then BIT1 else BIT0)
      ((if b1 then BIT1 else BIT0)
       ((if b2 then BIT1 else BIT0)
        ((if b3 then BIT1 else BIT0)
         ((if b4 then BIT1 else BIT0)
          ((if b5 then BIT1 else BIT0)
           ((if b6 then BIT1 else BIT0)
 	    ((if b7 then BIT1 else BIT0)
 	     _0))))))))`;;

let NUM_OF_CHAR_CLAUSES =
  let chars =
    let app f acc = itlist (fun b acc -> mk_comb(f,b) :: acc) [`F`; `T`] acc in
    let bind f = itlist app f [] in
    funpow 8 bind [`ASCII`] in
  let conv = REWR_CONV NUM_OF_CHAR THENC REWRITE_CONV[ARITH_ZERO] in
  let mk_thm tm = conv(mk_comb(`NUM_OF_CHAR`,tm)) in
  end_itlist CONJ (map mk_thm chars);;

let NUM_OF_CHAR_LT_256 = prove
  (`!c. NUM_OF_CHAR c < 256`,
   MATCH_MP_TAC char_INDUCT THEN
   REWRITE_TAC[FORALL_BOOL_THM; NUM_OF_CHAR_CLAUSES] THEN NUM_REDUCE_TAC);;

let NUM_OF_CHAR_CONV = GEN_REWRITE_CONV I [NUM_OF_CHAR_CLAUSES];;

(* ------------------------------------------------------------------------- *)
(* CHAR_OF_NUM and NUM_OF_CHAR are inverses of each other.                   *)
(* ------------------------------------------------------------------------- *)

let NUM_OF_CHAR_OF_NUM = prove
 (`!n. n IN 0..255 ==> NUM_OF_CHAR (CHAR_OF_NUM n) = n`,
  CONV_TAC (ONCE_DEPTH_CONV NUMSEG_CONV) THEN
  REWRITE_TAC[FORALL_IN_INSERT; NOT_IN_EMPTY] THEN REPEAT CONJ_TAC THEN
  CONV_TAC (LAND_CONV (RAND_CONV CHAR_OF_NUM_CONV THENC NUM_OF_CHAR_CONV)) THEN
  REFL_TAC);;

let CHAR_OF_NUM_OF_CHAR = prove
 (`!c. CHAR_OF_NUM (NUM_OF_CHAR c) = c`,
  MATCH_MP_TAC char_INDUCT THEN
  REPEAT (MATCH_MP_TAC bool_INDUCT THEN CONJ_TAC) THEN
  CONV_TAC (LAND_CONV (RAND_CONV NUM_OF_CHAR_CONV THENC CHAR_OF_NUM_CONV)) THEN
  REFL_TAC);;

(* ------------------------------------------------------------------------- *)
(* NUM_OF_STRING                                                             *)
(* ------------------------------------------------------------------------- *)

let NUM_OF_STRING = new_recursive_definition list_RECURSION
  `NUM_OF_STRING "" = 0 /\
   (!c cs. NUM_OF_STRING (CONS c cs) =
           256 * NUM_OF_STRING cs + NUM_OF_CHAR c + 1)`;;

(* ------------------------------------------------------------------------- *)
(* STRING_OF_NUM                                                             *)
(* ------------------------------------------------------------------------- *)

let STRING_OF_NUM = define
  `STRING_OF_NUM n =
   if n = 0
   then ""
   else CONS (CHAR_OF_NUM ((n - 1) MOD 256)) (STRING_OF_NUM ((n - 1) DIV 256))`;;

(* ------------------------------------------------------------------------- *)
(* STRING_OF_NUM and NUM_OF_STRING are inverses of each other.               *)
(* ------------------------------------------------------------------------- *)

let STRING_OF_NUM_OF_STRING = prove
 (`!s. STRING_OF_NUM (NUM_OF_STRING s) = s`,
  MATCH_MP_TAC list_INDUCT THEN CONJ_TAC THENL
  [REWRITE_TAC[NUM_OF_STRING] THEN REWRITE_TAC[STRING_OF_NUM];
   ALL_TAC] THEN
  INTRO_TAC "![c] [cs]; ind" THEN REWRITE_TAC[NUM_OF_STRING] THEN
  ONCE_REWRITE_TAC[STRING_OF_NUM] THEN
  REWRITE_TAC[ARITH_RULE
    `~(256 * NUM_OF_STRING cs + NUM_OF_CHAR c + 1 = 0)`] THEN
  REWRITE_TAC[ARITH_RULE
    `(256 * NUM_OF_STRING cs + NUM_OF_CHAR c + 1) - 1 =
     NUM_OF_STRING cs * 256 + NUM_OF_CHAR c`] THEN
  BINOP_TAC THENL
  [REWRITE_TAC[MOD_MULT_ADD] THEN SIMP_TAC[MOD_LT; NUM_OF_CHAR_LT_256] THEN
   REWRITE_TAC[CHAR_OF_NUM_OF_CHAR];
   SIMP_TAC[DIV_MULT_ADD; ARITH_EQ] THEN
   SIMP_TAC[DIV_LT; NUM_OF_CHAR_LT_256] THEN ASM_REWRITE_TAC[ADD_0]]);;

let NUM_OF_STRING_OF_NUM = prove
 (`!n. NUM_OF_STRING (STRING_OF_NUM n) = n`,
  MATCH_MP_TAC num_WF THEN GEN_TAC THEN ASM_CASES_TAC `n = 0` THENL
  [POP_ASSUM SUBST_VAR_TAC THEN ONCE_REWRITE_TAC[STRING_OF_NUM] THEN
   REWRITE_TAC[NUM_OF_STRING];
   POP_ASSUM (LABEL_TAC "nz") THEN INTRO_TAC "ind"] THEN
  ONCE_REWRITE_TAC[STRING_OF_NUM] THEN ASM_REWRITE_TAC[NUM_OF_STRING] THEN
  SUBGOAL_THEN `NUM_OF_CHAR (CHAR_OF_NUM ((n - 1) MOD 256)) = (n - 1) MOD 256`
    SUBST1_TAC THENL
  [MATCH_MP_TAC NUM_OF_CHAR_OF_NUM THEN REWRITE_TAC[IN_NUMSEG] THEN ARITH_TAC;
   REMOVE_THEN "ind" (MP_TAC o SPEC `(n - 1) DIV 256`) THEN ASM_ARITH_TAC]);;
